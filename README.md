# JIRA Admin Helper Scripts

This repository contains scripts that may help you in maintenance of your JIRA instance. 

## Prerequisites

1. node.js, version 5.11.0+
2. ```git clone git@bitbucket.org:atlassian/jira-admin-helper-scripts.git```
3. ```cd jira-admin-helper-scripts```
4. ```npm install```

## Query workflow permissions before upgrade to JIRA Server 7.3.0

JIRA 7.3.0 introduced a change in workflow editing that allow users with ```ADMINISTER_PROJECT``` permission to edit isolated workflows.

This command will print out user names, groups, projects and workflows affected by this change

    node query-project-admin-workflow-permissions.js

Sample output:

    ==== Results ====
    Project: qw
	    Lead: admin
    	    Workflow: jira
	    Granted:  jira-software-users (group)
    Project: qwe
	    Lead: fred
    	    Workflow: QWE: Project Management Workflow
	    Granted:  jira-software-users (group) fred (projectLead)