var Q = require('q');
var _ = require('underscore');
var prompt = require('prompt');
var Client = require('node-rest-client').Client;
var ProgressBar = require('progress');

Q.longStackSupport = true;

var client;
var contextPath;

prompt.start();

var promptSchema = {
    properties: {
        jiraAddress: {
            required: true,
            default: "http://localhost:8090/jira"
        },
        adminUsername: {
            required: true,
            default: "admin",
        },
        adminPassword: {
            required: true,
            default: "admin",
            hidden: true
        }
    }
};

prompt.get(promptSchema, function (err, result) {
    client = new Client({ user: result.adminUsername, password: result.adminPassword });
    contextPath = result.jiraAddress;

    /* Build list of groups and users with sysadmin/admin permission */
    var AdminGroups = $get(contextPath + "/rest/api/2/groups/picker?maxResults=10000000")
        .then(response => response.groups.filter(group => group.labels.some(label => label.type === "ADMIN" || label.type === "SYSADMIN")).map(group => group.name));
    var AdminUsers = AdminGroups.then(groups =>
        Q.all(groups.map(group =>
            $get(contextPath + "/rest/api/2/group/member?groupname=" + group).then(members => members.values.map(value => value.name))))
    ).then((...users) => _.flatten(users));

    AdminUsers.tap(users => AdminGroups.tap(groups => queryPermissions(groups, users))).fail(err => console.log("Unable to get admin groups and users", err));
});

function queryPermissions(groups, users) {
    var REST_PROJECT = contextPath + "/rest/api/2/project/";
    var bar;

    var workflowMap = {};

    /* For each project get workflow scheme and populate workflowMap with its mappings */
    $get(REST_PROJECT)
        .then(projects => {
            console.log("Processing " + projects.length + " projects, this may take a while");

            bar = new ProgressBar(':bar', { total: projects.length });


            projects.reduce(reduceProjects, Q()).then(() => {
                console.log("DONE! " + projects.length + " projects");
                console.log("Detected admin groups:", groups);
                console.log("==== Results ====");
                _.each(workflowMap, (projectGrants, workflowName) => {
                    /* only one project in workflow map means it is isolated */
                    if (_.uniq(projectGrants.map(grant => grant.project.name)).length === 1) {
                        projectGrants.forEach(grant => {
                            console.log(
                                "Project: " + grant.project.name,
                                "\n\tLead: " + grant.project.lead.name,
                                "\n\tWorkflow: " + workflowName,
                                "\n\tGranted: ", _.flatten(grant.grants).join(' '))
                        })
                    }
                });
            })
        }).fail((err) => {
            console.error("Querying permissions failed:", err);
        });


    function reduceProjects(r, project, currentIndex, projects) {
        return r.then(() =>
            $get(REST_PROJECT + project.id + "/permissionscheme?expand=permissions")
            .then(expand => getGrants(expand, project)
                .then(grants => grants.length == 0 ||
                    $get(contextPath + "/rest/projectconfig/latest/workflowscheme/" + project.key).then(workflowscheme => {
                        return $get(REST_PROJECT + project.id).then(project =>
                            workflowscheme.mappings.forEach(mapping => {
                                (workflowMap[mapping.name] = (workflowMap[mapping.name] || [])).push({
                                    project: project,
                                    grants: grants
                                })
                            })
                        );
                    })
                ).tap(() => bar.tick())
            )
        )
    }

    function getGrants(expand, project) {
        let all = Q.all(expand.permissions
            .filter(perm => perm.permission === "ADMINISTER_PROJECTS")
            .map(holder => mapHolder(holder, project)));
        return all.then((grants) =>
            grants.filter(grant => grant[0] && grant[0].length > 0)
        );
    }

    function mapHolder(holder, project) {
        if (holder.holder.type === "applicationRole" && !holder.holder.parameter) {
            return Q(["Any loggedin user", '(' + holder.holder.type + ')']);
        } else if (holder.holder.type === "projectRole" && holder.holder.parameter) {
            return $get(REST_PROJECT + project.id + "/role/" + holder.holder.parameter).then(result => [result.actors.filter(actor => validateActor(actor)).map(actor => actor.name), '(' + holder.holder.type + '::' + result.name + ')']);
        } else if (holder.holder.type === "applicationRole") {
            return $get(contextPath + "/rest/api/2/applicationrole/" + holder.holder.parameter).then(role => [_.difference(role.groups, groups), '(' + holder.holder.type + '::' + holder.holder.parameter + ')']);
        } else if (holder.holder.type === "user") {
            return Q([users.indexOf(holder.holder.parameter) === -1 ? holder.holder.parameter : '', '(user)']);
        } else if (holder.holder.type === "group") {
            return Q([groups.indexOf(holder.holder.parameter) === -1 ? holder.holder.parameter : '', '(group)']);
        } else if (holder.holder.type === "projectLead") {
            return $get(REST_PROJECT + project.id).then(project => [users.indexOf(project.lead.name) === -1 ? project.lead.name : '', '(projectLead)']);
        } else {
            return Q([(holder.holder[holder.holder.expand] || {
                name: holder.holder.parameter
            }).name, '(' + holder.holder.type + ')']);
        }
    }

    function validateActor(actor) {
        if (actor.type === 'atlassian-group-role-actor') {
            return groups.indexOf(actor.name) === -1;
        } else if (actor.type === 'atlassian-user-role-actor') {
            return users.indexOf(actor.name) === -1;
        } else {
            return false;
        }
    }
};

var $get = function(uri) {
    var result = Q.defer();

    client.get(uri, {}, function (data, response) {
        if (response.statusCode === 200) {
            result.resolve(data);
        } else {
            throw new Error(response.statusCode + " on " + uri);
        }
    });

    return result.promise;
};